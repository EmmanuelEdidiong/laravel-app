@extends('layouts.admin')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.css">

@endsection

@section('content')

    <h1>Upload Media</h1>

    {{ Form::open(['method'=>'POST', 'action'=>'AdminMediaController@store', 'class'=>'dropzone']) }}

    <!-- <div class="form-group">
        {{ Form::submit('Upload media', ['class'=>'form-control'])}}
    </div> -->

    {{ Form::close() }}

   

@endsection

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js"></script>

@endsection