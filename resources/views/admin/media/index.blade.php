@extends('layouts.admin')

@section('content')

    @if(Session::has('deleted_media'))
        <p class="bg-danger">{{session('deleted_media')}}</p>
    @endif

    <h1>Media</h1>

    <style>
        
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>

    <!-- bulk delete form -->
    <form action="/delete/media" method="post" class="form-inline">
        {{csrf_field()}}

        {{method_field('delete')}}
        <div class="form-group">
            <select name="checkBoxArray" id="" class="form-control">
                <option value="">Delete</option>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" name="delete_all" class="btn-primary">
        </div>

        <table style="width:100%; margin-top: 10px">
            <tr>
                <th><input type="checkbox" id="options"></th>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Delete</th>
            </tr>

            @if($photos)
                @foreach($photos as $photo)
            <tr>
                <td><input type="checkbox" name="checkBoxArray[]" value="{{$photo->id}}"></td>
                <td>{{$photo->id}}</td>
                <td><img height="50" src="{{$photo->file ? $photo->file : 'No image found'}}"></td>
                <td>{{$photo->created_at ? $photo->created_at->diffForHumans() : 'No date'}}</td>
                <td>
                
                <!-- {{ Form::open(['method'=>'DELETE', 'action'=>['AdminMediaController@destroy', $photo->id]]) }}
                    
                    <div class="form-group">
                        {{ Form::submit('Delete image', ['class'=>'btn btn-danger'])}}
                    </div>
            
                {{ Form::close() }} -->

                <!-- single delete form -->
                <input type="hidden" name="photo" value="{{$photo->id}}">
                <div class="form-group">
                    <input type="submit" name="delete_single" value="Delete" class="btn-danger">
                </div>

                </td>
            </tr>
                @endforeach
            @endif
        </table>

    </form>
    

@endsection

@section('scripts')

    <script>
        // $(document).ready(function() {
        //    $('#options').click(function() {
        //        if(this.checked) {
        //            $('.checkBoxes').each(function() {
        //                this.checked = true;
        //            });
        //         } else {
        //             $('.checkBoxes').each(function() {
        //                 this.checked = true;
        //             });
        //         }
        //     });
        // });

        $('#options').click(function () {    
            $('input:checkbox').prop('checked', this.checked);    
        });
    </script>

@endsection