@extends('layouts.admin')

@section('content')

    @if(Session::has('deleted_category'))
        <p class="bg-danger">{{session('deleted_category')}}</p>
    @endif

    <h1>Categories</h1>

    <div class="row">
        @include('includes.form_error')
    </div>

    <style>
        
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>

    <div class="row">

        <div class="col-sm-6">
            
            {{ Form::open(['method'=>'POST', 'action'=>'AdminCategoriesController@store']) }}
                
                <div class="form-group">
                    {{ Form::label('name', 'Name:') }}
                    {{ Form::text('name', null, ['class'=>'form-control'])}}
            
                </div>
                <div class="form-group">
                    {{ Form::submit('Create Category', ['class'=>'btn btn-primary col-sm-6'])}}
                </div>

            {{ Form::close() }}

        </div>
    
        <div class="col-sm-6">
        
            <table style="width:100%">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Created</th>
                </tr>

                @if($categories)
                    @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td><a href="{{route('categories.edit', $category->id)}}">{{$category->name}}</a></td>
                    <td>{{$category->created_at ? $category->created_at->diffForHumans() : 'No date'}}</td>
                </tr>
                    @endforeach
                @endif
            </table>

        </div>

    </div>



@endsection