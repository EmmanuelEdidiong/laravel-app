@extends('layouts.admin')

@section('content')

    <h1>Create Users</h1>

    <div class="row">
        @include('includes.form_error')
    </div>

    <div class="row">

        {{ Form::open(['method'=>'POST', 'action'=>'AdminUsersController@store', 'files'=>true]) }}
            
            <div class="form-group">
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', null, ['class'=>'form-control'])}}
        
            </div>
            <div class="form-group">
                {{ Form::label('email', 'Email:') }}
                {{ Form::email('email', null, ['class'=>'form-control'])}}
        
            </div>
            <div class="form-group">
                {{ Form::label('role_id', 'Role:') }}
                {{ Form::select('role_id', [''=>'choose options'] + $roles, null, ['class'=>'form-control'])}}
            </div>
            <div class="form-group">
                {{ Form::label('is_active', 'Status:') }}
                {{ Form::select('is_active', [1 => 'Active', 0 => 'Not Active'], 0, ['class'=>'form-control'])}} <!-- the null can serve as default value by change it to 0 or 1 -->
            </div>
            <div class="form-group">
                {{ Form::label('photo_id', 'Upload Photo:') }}
                {{ Form::file('photo_id', ['class'=>'form-control'])}} <!-- the null can serve as default value by change it to 0 or 1 -->
            </div>
            <div class="form-group">
                {{ Form::label('password', 'Password:') }}
                {{ Form::password('password', ['class'=>'form-control'])}} <!-- the null can serve as default value by change it to 0 or 1 -->
            </div>
            <!-- <div class="form-group">
                {{ Form::file('file', ['class'=>'form-control'])}}
            
            </div> -->
            <div class="form-group">
                {{ Form::submit('Create User', ['class'=>'btn btn-primary'])}}
            </div>

            
            <!-- <input type="submit" name="submit"> -->
        {{ Form::close() }}
    </div>

@endsection