@extends('layouts.admin')

@section('content')

    <h1>Edit User</h1><br>

    <div class="row">
        @include('includes.form_error')
    </div>

    <div class="row">
        <div class="col-sm-2">
        
            <img src="{{$user->photo ? $user->photo->file : 'No Photo found'}}" height="100">

        </div>

        <div class="col-sm-10">
            {{ Form::model($user, ['method'=>'PATCH', 'action'=>['AdminUsersController@update', $user->id], 'files'=>true]) }}
                
                <div class="form-group">
                    {{ Form::label('name', 'Name:') }}
                    {{ Form::text('name', null, ['class'=>'form-control'])}}
            
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'Email:') }}
                    {{ Form::email('email', null, ['class'=>'form-control'])}}
            
                </div>
                <div class="form-group">
                    {{ Form::label('role_id', 'Role:') }}
                    {{ Form::select('role_id', [''=>'choose options'] + $roles, null, ['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{ Form::label('is_active', 'Status:') }}
                    {{ Form::select('is_active', [1 => 'Active', 0 => 'Not Active'], null, ['class'=>'form-control'])}} <!-- the null can serve as default value by change it to 0 or 1 -->
                </div>
                <div class="form-group">
                    {{ Form::label('photo_id', 'Upload Photo:') }}
                    {{ Form::file('photo_id', ['class'=>'form-control'])}} 
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Password:') }}
                    {{ Form::password('password', ['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{ Form::submit('Update User', ['class'=>'btn btn-primary col-sm-6'])}}
                </div>
            {{ Form::close() }}

            {{ Form::open(['method'=>'DELETE', 'action'=>['AdminUsersController@destroy', $user->id]]) }}
                
                <div class="form-group">
                    {{ Form::submit('Delete User', ['class'=>'btn btn-danger col-sm-6'])}}
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection