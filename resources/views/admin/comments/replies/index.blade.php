@extends('layouts.admin')

@section('content')

    <!-- @if(Session::has('deleted_media'))
        <p class="bg-danger">{{session('deleted_media')}}</p>
    @endif -->

    <h1>Replies</h1>

    <style>
        
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>

    @if(count($replies) > 0)

    <table style="width:100%">
        <tr>
            <th>Id</th>
            <th>Author</th>
            <th>Email</th>
            <th>Body</th>
            <th>Post</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Aprrove</th>
            <th>Delete</th>
        </tr>

            @foreach($replies as $reply)
        <tr>
            <td>{{$reply->id}}</td>
            <td>{{$reply->author}}</td>
            <td>{{$reply->email}}</td>
            <td>{{Str::limit($reply->body, 10)}}</td>
            <td><a href="{{route('home.post', $reply->comment->post->slug)}}">View post</a></td>
            <td>{{$reply->created_at->diffForHumans()}}</td>
            <td>{{$reply->updated_at->diffForHumans()}}</td>
            <td>
                @if($reply->is_active == 1)

                    {{ Form::open(['method'=>'PATCH', 'action'=>['CommentRepliesController@update', $reply->id]]) }}
            
                        <input type="hidden" name="is_active" value="0">
                        <div class="form-group">
                            {{ Form::submit('Unapprove', ['class'=>'btn btn-success'])}}
                        </div>

                    {{ Form::close() }}

                    @else

                    {{ Form::open(['method'=>'PATCH', 'action'=>['CommentRepliesController@update', $reply->id]]) }}
            
                        <input type="hidden" name="is_active" value="1">
                        <div class="form-group">
                            {{ Form::submit('Approve', ['class'=>'btn btn-info'])}}
                        </div>

                    {{ Form::close() }}

                @endif
            </td>
            <td>
                {{ Form::open(['method'=>'DELETE', 'action'=>['CommentRepliesController@destroy', $reply->id]]) }}
                
                    <div class="form-group">
                        {{ Form::submit('Delete', ['class'=>'btn btn-danger'])}}
                    </div>

                {{ Form::close() }}
            </td>
        </tr>
            @endforeach
    </table>
    
    @else
    <h3>No replies</h1>

    @endif

@endsection