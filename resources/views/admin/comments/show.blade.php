@extends('layouts.admin')

@section('content')

    <!-- @if(Session::has('deleted_media'))
        <p class="bg-danger">{{session('deleted_media')}}</p>
    @endif -->

    <h1>Comments</h1>

    <style>
        
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>

    @if(count($comments) > 0)

    <table style="width:100%">
        <tr>
            <th>Id</th>
            <th>Author</th>
            <th>Email</th>
            <th>Body</th>
            <th>Post</th>
            <th>Replies</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Aprrove</th>
            <th>Delete</th>
        </tr>

            @foreach($comments as $comment)
        <tr>
            <td>{{$comment->id}}</td>
            <td>{{$comment->author}}</td>
            <td>{{$comment->email}}</td>
            <td>{{Str::limit($comment->body, 10)}}</td>
            <td><a href="{{route('home.post', $comment->post->slug)}}">View post</a></td>
            <td><a href="{{route('replies.show', $comment->id)}}">View replies</a></td>
            <td>{{$comment->created_at->diffForHumans()}}</td>
            <td>{{$comment->updated_at->diffForHumans()}}</td>
            <td>
                @if($comment->is_active == 1)

                    {{ Form::open(['method'=>'PATCH', 'action'=>['PostCommentsController@update', $comment->id]]) }}
            
                        <input type="hidden" name="is_active" value="0">
                        <div class="form-group">
                            {{ Form::submit('Unapprove', ['class'=>'btn btn-success'])}}
                        </div>

                    {{ Form::close() }}

                    @else

                    {{ Form::open(['method'=>'PATCH', 'action'=>['PostCommentsController@update', $comment->id]]) }}
            
                        <input type="hidden" name="is_active" value="1">
                        <div class="form-group">
                            {{ Form::submit('Approve', ['class'=>'btn btn-info'])}}
                        </div>

                    {{ Form::close() }}

                @endif
            </td>
            <td>
                {{ Form::open(['method'=>'DELETE', 'action'=>['PostCommentsController@destroy', $comment->id]]) }}
                
                    <div class="form-group">
                        {{ Form::submit('Delete', ['class'=>'btn btn-danger'])}}
                    </div>

                {{ Form::close() }}
            </td>
        </tr>
            @endforeach
    </table>
    
    @else
    <h3>No comments</h1>

    @endif

@endsection