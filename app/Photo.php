<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //

    protected $fillable = [
        'file'
    ];

    protected $uploads = '/images/' ;

    public function getFileAttribute($photo) {
        return $this->uploads . $photo; //concatenates the photo 'images' directory instead of always typeing /images/... in src
    }
}
