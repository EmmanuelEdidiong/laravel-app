<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // to access the authenticated user we add the auth class on top
        $user = Auth::user();
        return view('front.home', compact('user')); //compact creates an array containing variables, so it allows values from the database to be called out


        //sessions
        // $request->session()->put(['emmanuel'=>'student']);
        // return $request->session()->all();
        // session(['emma'=>'your best student']);
        // return session('emma');
        // $request->session()->forget('emma'); //to delete a session
       
        // $request->session()->flush(); // to delete all sessions

        // return $request->session()->all(); // returns all sessions
        

        // flashing data
        // $request->session()->flash('message', 'Post has been created');

        // return $request->session()->get('message');

        // $request->session()->reflash(); //to reshow the flash again

        // $request->session()->keep('message'); // to keep the flash data around
    }
}
