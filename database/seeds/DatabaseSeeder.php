<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

        // DB::table('users')->insert(
        //     [
        //         'name'=>Str::random(10),
        //         'role_id'=>2,
        //         'is_active'=>1,
        //         'email'=>Str::random(10). '@gmail.com',
        //         'password'=>Hash::make('password'),
        //         'created_at'=>now(),
        //         'updated_at'=>now()
        //         // 'password'=>bcrypt('secret')

        //     ]
        // );

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
        DB::table('roles')->truncate();
        DB::table('categories')->truncate();
        DB::table('photos')->truncate();
        DB::table('comments')->truncate();
        DB::table('comment_replies')->truncate();

        factory(App\User::class, 10)->create()->each(function($user){
            
            $user->posts()->save(factory(App\Post::class)->make());

        });

        factory(App\Role::class, 3)->create();

        factory(App\Category::class, 5)->create();

        factory(App\Photo::class, 3)->create();

        factory(App\Comment::class, 10)->create()->each(function($comment){
            
            $comment->replies()->save(factory(App\CommentReply::class)->make());

        });

    }
}
