<?php

// use App\Role; // called role model
// use App\User;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
    // $user = Auth::user();

    // if($user->isAdmin()) {
    //     echo 'this user is an Administrator';
    // }

    // if(Auth::check()) {
    //     return 'the user is logged in'; //another of checking the user is logged in
    // }

    //or
    // $username = 'dora';
    // $password = '3tft3rftgt';
    // if(Auth::attempt(['username'=>$username,'password'=>$password])) {
    //     return redirect()->intended('/admin');
    // }

// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


// Route::get('/admin/user/roles', ['middleware'=>'role', function() {
//     return "Middleware role";
// }]);

Route::get('/post/{id}', ['as'=>'home.post', 'uses'=>'AdminPostsController@post']);

Route::group(['middleware'=>'isAdmin'], function(){

    Route::get('/admin', 'AdminController@index');
    
    Route::resource('/admin/users', 'AdminUsersController');
    
    Route::resource('/admin/posts', 'AdminPostsController');

    Route::resource('/admin/categories', 'AdminCategoriesController');

    Route::resource('/admin/media', 'AdminMediaController');

    // Route::get('/admin/media/upload', ['as'=>'media.upload', 'uses'=>'AdminMediaController@store']);

    Route::delete('/delete/media', 'AdminMediaController@deleteMedia');

    Route::resource('/admin/comments', 'PostCommentsController');

    Route::resource('/admin/comment/replies', 'CommentRepliesController');




});

Route::group(['middleware'=>'auth'], function() {

    Route::post('/comment/reply', 'CommentRepliesController@createReply');
    
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
